/*
 *  Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2024 - 3DS - Antoine ELIAS
 *
 * For more information, see the COPYING file which you should have received
 * along with this program.
 */

#ifndef __CLASSDEF_HXX__
#define __CLASSDEF_HXX__

#include <map>
#include <tuple>
#include "callable.hxx"
#include "function.hxx"
#include "internal.hxx"
#include "arguments.hxx"

namespace types
{

enum AccessModifier
{
    NONE,
    PRIVATE,
    PROTECTED,
    PUBLIC
};

struct OBJ_ATTR
{
    ARG arg;
    AccessModifier access;
    bool isStatic;
    Callable* callable;
};

class EXTERN_AST Classdef : public types::InternalType
{
public:
    Classdef(const std::wstring& name,
        std::map<std::wstring, OBJ_ATTR>& properties,
        std::map<std::wstring, OBJ_ATTR>& methods,
        std::map<std::wstring, std::vector<types::InternalType*>>& enumerations,
        std::vector<std::wstring>& superclass);

    virtual ~Classdef();

    ScilabType getType(void) { return ScilabClassdef; }
    ScilabId getId(void) { return IdClassdef; }

    bool isClassdef() { return true; }
    bool toString(std::wostringstream& ostr)
    {
        if (properties.size() > 0)
        {
            ostr << name << L" with properties :" << std::endl;
            for (auto&& p : properties)
            {
                ostr << "    " << p.first << std::endl;
            }
        }

        if (methods.size() > 0)
        {
            ostr << name << L" with methods:" << std::endl;
            for (auto&& m : methods)
            {
                ostr << "    " << m.first << std::endl;
            }
        }

        if (enumerations.size() > 0)
        {
            ostr << name << L" with enumeration :" << std::endl;
            for (auto&& e : enumerations)
            {
                ostr << "    " << e.first << std::endl;
            }
        }

        return true;
    }

    bool isA(const std::wstring& type)
    {
        if (type == L"classdef" || type == name)
        {
            return true;
        }

        for (auto&& s : supers)
        {
            if (std::get<1>(s)->isA(type))
            {
                return true;
            }
        }

        return false;
    }

    virtual std::wstring getTypeStr() const override { return name; };
    virtual std::wstring getShortTypeStr() const override { return name; }

    InternalType* clone(void)
    {
        return this;
    }

    std::wstring getName() { return name; }
    std::map<std::wstring, OBJ_ATTR> getProperties() { return properties; }
    std::map<std::wstring, OBJ_ATTR> getMethods() { return methods; }

    std::vector<std::wstring> getPublicProperties();
    std::vector<std::wstring> getPublicMethods();

    bool getAccessProperty(const std::wstring& name, AccessModifier& access);

    InternalType* instantiateProperty(const std::wstring& name, const OBJ_ATTR& attr);
    Callable* Classdef::instantiateMethod(const std::wstring& name, const OBJ_ATTR& attr, bool isStatic = false);

    std::vector<std::tuple<std::wstring, Classdef*>> getSuperclass();

    bool isInvokable() const
    {
        return true;
    }

    bool invoke(typed_list& in, optional_list& opt, int _iRetCount, typed_list& out, const ast::Exp& e);
    bool extract(const std::wstring& name, InternalType*& out);
    Classdef* insert(typed_list* _pArgs, InternalType* _pSource);

    std::wstring getPropertyClassdef(const std::wstring& name);
    std::wstring getMethodClassdef(const std::wstring& name);

    void addStaticProperty(const std::wstring& name, const OBJ_ATTR& attr);
    void addStaticMethod(const std::wstring& name, const OBJ_ATTR& attr);
    InternalType* getStatic(const std::wstring& name);
    bool hasStatic(const std::wstring& name);
    bool setStatic(const std::wstring& name, InternalType* pIT);

  private:
    std::wstring name;
    std::map<std::wstring, OBJ_ATTR> properties;
    std::map<std::wstring, OBJ_ATTR> methods;
    std::map<std::wstring, std::vector<types::InternalType*>> enumerations;
    std::map<std::wstring, InternalType*> instances;
    std::vector<std::tuple<std::wstring, Classdef*>> supers;
};
} // namespace types

#endif /* __CLASSDEF_HXX__ */
