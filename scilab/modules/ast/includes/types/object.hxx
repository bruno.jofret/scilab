/*
 *  Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2024 - 3DS - Antoine ELIAS
 *
 * For more information, see the COPYING file which you should have received
 * along with this program.
 */

#ifndef __OBJECT_HXX__
#define __OBJECT_HXX__

#include <map>
#include "function.hxx"
#include "classdef.hxx"
#include "double.hxx"
#include "user.hxx"

namespace types
{
class EXTERN_AST Object : public UserType
{
public:
    Object(Classdef* classdef);
    Object(const Object& obj);

    virtual ~Object();

    ScilabType getType(void) { return ScilabObject; }
    ScilabId getId(void) { return IdObject; }

    bool isObject() { return true; }
    bool isA(const std::wstring& type)
    {
        if (type == L"object" || type == def->getName())
        {
            return true;
        }

        return def->isA(type);
    }

    std::wstring getTypeStr() const override { return def->getName(); };
    std::wstring getShortTypeStr() const override { return def->getName(); }

    bool isAssignable() { return true; }
    bool isInvokable() const override { return true; }

    bool hasMethod(const std::wstring& method);
    Callable* getMethod(const std::wstring& name);
    virtual bool hasGetFields() { return true; }

    // overload this method if hasGetFields method return true
    virtual types::String* getFields();

    bool hasProperty(const std::wstring& property);
    InternalType* getProperty(const std::wstring& name);
    std::map<std::wstring, types::InternalType*> getProperties() const { return properties; }

    Function::ReturnValue callConstructor(typed_list& in, optional_list& opt, int _iRetCount, typed_list& out);
    Function::ReturnValue callMethod(const std::wstring& method, typed_list& in, optional_list& opt, int _iRetCount, typed_list& out);
    Function::ReturnValue callSuperclassContructor(Classdef* super, typed_list& in, optional_list& opt, int _iRetCount, typed_list& out);
    bool invoke(typed_list& in, optional_list& /*opt*/, int /*_iRetCount*/, typed_list& out, const ast::Exp& /*e*/) override;

    bool extract(const std::wstring& name, InternalType*& out);
    Object* insert(typed_list* _pArgs, InternalType* _pSource) override;

    Object* clone()
    {
        //IncreaseRef();
        return this;
        //return new Object(*this);
    }

    void loadClassdef(Classdef* def, int level = 0);
    Classdef* getClassdef() const { return def; }

    void addHelpers();
    void addHelper(const std::wstring& name, Function::ReturnValue (Object::*_pFunc)(typed_list&, int, typed_list&));

protected:
    types::Function::ReturnValue object_disp(types::typed_list& in, int _iRetCount, types::typed_list& out);
    types::Function::ReturnValue object_eq(types::typed_list& in, int _iRetCount, types::typed_list& out);
    types::Function::ReturnValue object_ne(types::typed_list& in, int _iRetCount, types::typed_list& out);
    types::Function::ReturnValue object_outline(types::typed_list& in, int _iRetCount, types::typed_list& out);
    types::Function::ReturnValue object_clone(types::typed_list& in, int _iRetCount, types::typed_list& out);
    types::Function::ReturnValue object_delete(types::typed_list& in, int _iRetCount, types::typed_list& out);

    bool getAccessProperty(const std::wstring& name, AccessModifier& access);
    bool getAccessMethod(const std::wstring& name, AccessModifier& access);

    Object* getInstance(const std::wstring& name);

private:
    Classdef* def;
    std::map<std::wstring, types::InternalType*> properties;
    std::map<std::wstring, types::Callable*> methods;
    std::set<std::wstring> hidden;
    std::stack<std::wstring> scope;
    std::map<std::wstring, types::Object*> instances;
};
} // namespace types

#endif /* __OBJECT_HXX__ */