// ============================================================================
// Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2023 - Dassault Systemes - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

classdef Foo
    enumeration
        A
        B
        C
    end
end

classdef Colors
   properties
      R
      G
      B
   end
   methods
      function c = Colors(r, g, b)
         c.R = r; c.G = g; c.B = b;
      end
   end
   enumeration
      Blueish   (18/255,104/255,179/255)
      Reddish   (237/255,36/255,38/255)
      Greenish  (155/255,190/255,61/255)
      Purplish  (123/255,45/255,116/255)
      Yellowish (1,199/255,0)
      LightBlue (77/255,190/255,238/255)
   end
end

classdef Colors
   properties
      R = 0
      G = 0
      B = 0
   end
   methods
      function c = Colors(r, g, b)
         c.R = r; c.G = g; c.B = b;
      end
   end
   enumeration
      Blueish   (18/255,104/255,179/255)
      Reddish   (237/255,36/255,38/255)
      Greenish  (155/255,190/255,61/255)
      Purplish  (123/255,45/255,116/255)
      Yellowish (1,199/255,0)
      LightBlue (77/255,190/255,238/255)
   end
end

classdef Cars < CarPainter
   enumeration
      Hybrid (2,'Manual',55,Colors.Reddish)
      Compact(4,'Manual',32,Colors.Greenish)
      MiniVan(6,'Automatic',24,Colors.Blueish)
      SUV    (8,'Automatic',12,Colors.Yellowish)
   end
   properties (SetAccess = private)
      Cylinders
      Transmission
      MPG
      Color
   end
   methods
      function obj = Cars(cyl,trans,mpg,colr)
         obj.Cylinders = cyl;
         obj.Transmission = trans;
         obj.MPG = mpg;
         obj.Color = colr;
      end
      function paint(obj,colorobj)
         if isa(colorobj,'Colors')
            obj.Color = colorobj;
         else
            //[~,cls] = enumeration('Colors');
            [_,cls] = enumeration('Colors');
            disp('Not an available color')
            disp(cls)
         end
      end
   end
end

/*
classdef CarPainter < handle
   methods (Abstract)
      paint(carobj,colorobj)
   end
end
*/

classdef BasicClass
    properties
        Value {mustBeNumeric}
    end
    methods
        function obj = BasicClass(val)
            if nargin == 1
                obj.Value = val;
            end
        end
        function r = roundOff(obj)
            r = round([obj.Value],2);
        end
        function r = multiplyBy(obj,n)
            r = [obj.Value] * n;
        end
        function r = plus(o1,o2)
            r = [o1.Value] + [o2.Value];
        end
    end
end