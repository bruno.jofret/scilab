/*
 *  Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2024 - 3DS - Antoine ELIAS
 *
 * For more information, see the COPYING file which you should have received
 * along with this program.
 */

#include "classdef.hxx"
#include "object.hxx"
#include "context.hxx"
#include "runvisitor.hxx"

extern "C"
{
#include "sciprint.h"
}

namespace types
{
Classdef::Classdef(const std::wstring& name,
        std::map<std::wstring, OBJ_ATTR>& properties,
        std::map<std::wstring, OBJ_ATTR>& methods,
        std::map<std::wstring, std::vector<types::InternalType*>>& enumerations, 
        std::vector<std::wstring>& superclass)
    : name(name), properties(properties), methods(methods), enumerations(enumerations)
{
    for (auto&& s : superclass)
    {
        Classdef* def = symbol::Context::getInstance()->getClassdef(s);
        if (def)
        {
            supers.push_back({s, def});
        }
        else
        {
            char msg[128];
            os_sprintf(msg, _("'%s' does not exist\n"), scilab::UTF8::toUTF8(s).data());
            throw ast::InternalError(scilab::UTF8::toWide(msg));
        }
    }

    //static
    for (auto&& p : properties)
    {
        if (p.second.isStatic)
        {
            addStaticProperty(p.first, p.second);
        }
    }

    for (auto&& m : methods)
    {
        if (m.second.isStatic)
        {
            addStaticMethod(m.first, m.second);
        }
    }
}

Classdef::~Classdef()
{
    //sciprint("delete Classdef\n");
    for (auto&& i : instances)
    {
        i.second->DecreaseRef();
        i.second->killMe();
    }

    for (auto&& e : enumerations)
    {
        for (auto&& it : e.second)
        {
            it->DecreaseRef();
            it->killMe();
        }
    }
}

std::vector<std::tuple<std::wstring, Classdef*>> Classdef::getSuperclass()
{
    return supers;
}

bool Classdef::invoke(typed_list& in, optional_list& opt, int _iRetCount, typed_list& out, const ast::Exp& e)
{
    if (methods.find(name) != methods.end())
    {
        if (methods[name].access != AccessModifier::PUBLIC)
        {
            InternalType* pIT = symbol::Context::getInstance()->getCurrentObject();
            if (pIT == nullptr || (pIT->isClassdef() && pIT != this) || (pIT->isObject() && pIT->getAs<Object>()->getClassdef() != this))
            {
                wchar_t szError[128];
                os_swprintf(szError, 128, _W("Wrong call: constructor of '%ls' is not accessible.\n").c_str(), name.data());
                throw ast::InternalError(szError);
            }
        }
    }

    Object* obj = new Object(this);
    obj->IncreaseRef();
    obj->callConstructor(in, opt, _iRetCount, out);
    obj->DecreaseRef();
    out.push_back(obj);
    return true;
}

Classdef* Classdef::insert(typed_list* _pArgs, InternalType* _pSource)
{
    if (_pArgs->size() == 1 && (*_pArgs)[0]->isString())
    {
        std::wstring field((*_pArgs)[0]->getAs<types::String>()->get(0));
        //InternalType* pIT = symbol::Context::getInstance()->getCurrentObject();
        AccessModifier access;
        if (getAccessProperty(field, access))
        {
            if (access == AccessModifier::PUBLIC || 
                (symbol::Context::getInstance()->getCurrentObject() == this && access != AccessModifier::NONE))
            {
                setStatic(field, _pSource);
            }
            else
            {
                wchar_t szError[128];
                os_swprintf(szError, 128, _W("Wrong insertion: property '%ls' is not accessible.\n").c_str(), field.data());
                throw ast::InternalError(szError);
            }
        }
        else
        {
            wchar_t szError[128];
            os_swprintf(szError, 128, _W("Wrong insertion: property '%ls' does not exist.\n").c_str(), field.data());
            throw ast::InternalError(szError);
        }
    }
    else
    {
        wchar_t szError[128];
        os_swprintf(szError, 128, _W("Wrong insertion: invalid index.\n").c_str());
        throw ast::InternalError(szError);
    }

    return this;
}

bool Classdef::getAccessProperty(const std::wstring& name, AccessModifier& access)
{
    InternalType* pIT = symbol::Context::getInstance()->getCurrentObject();
    auto p = properties.find(name);
    if (p != properties.end())
    {
        access = p->second.access;
        return true;
    }

    for (auto&& s : supers)
    {
        if (std::get<1>(s)->getAccessProperty(name, access))
        {
            switch (access)
            {
                case types::PRIVATE:
                    access = AccessModifier::NONE;
                    break;
                case types::PROTECTED:
                    access = AccessModifier::PRIVATE;
                    break;
            }

            return true;
        }
    }
    return false;
}

bool Classdef::extract(const std::wstring& name, InternalType*& out)
{
    auto p = instances.find(name);
    if (p != instances.end())
    {
        out = p->second;
        return true;
    }

    auto e = enumerations.find(name);
    if (e != enumerations.end())
    {
        optional_list opt;
        typed_list out1;

        Object* obj = new Object(this);
        obj->IncreaseRef();
        obj->callConstructor(e->second, opt, 0, out1);
        instances[e->first] = obj;

        out = obj;
        return true;
    }
    
    return false;
}

std::wstring Classdef::getPropertyClassdef(const std::wstring& name)
{
    auto p = properties.find(name);
    if (p != properties.end())
    {
        return this->name;
    }

    for (auto&& s : supers)
    {
        std::wstring p = std::get<1>(s)->getPropertyClassdef(name);
        if (p != L"")
        {
            return p;
        }
    }
    return L"";
}

std::wstring Classdef::getMethodClassdef(const std::wstring& name)
{
    auto p = methods.find(name);
    if (p != methods.end())
    {
        return this->name;
    }

    for (auto&& s : supers)
    {
        std::wstring p = std::get<1>(s)->getMethodClassdef(name);
        if (p != L"")
        {
            return p;
        }
    }
    return L"";
}

std::vector<std::wstring> Classdef::getPublicProperties()
{
    std::vector<std::wstring> props;
    for (auto&& p : properties)
    {
        if (p.second.access == AccessModifier::PUBLIC)
        {
            props.push_back(p.first);
        }
    }

    for (auto&& s : supers)
    {
        auto p = std::get<1>(s)->getPublicProperties();
        props.insert(props.end(), p.begin(), p.end());
    }

    return props;
}

std::vector<std::wstring> Classdef::getPublicMethods()
{
    std::vector<std::wstring> m;
    for (auto&& p : methods)
    {
        if (p.second.access == AccessModifier::PUBLIC)
        {
            m.push_back(p.first);
        }
    }

    for (auto&& s : supers)
    {
        auto p = std::get<1>(s)->getPublicMethods();
        m.insert(m.end(), p.begin(), p.end());
    }

    return m;
}

void Classdef::addStaticProperty(const std::wstring& name, const OBJ_ATTR& attr)
{
    instances[name] = instantiateProperty(name, attr);
    instances[name]->IncreaseRef();
}

void Classdef::addStaticMethod(const std::wstring& name, const OBJ_ATTR& attr)
{
    instances[name] = instantiateMethod(name, attr, true);
    instances[name]->IncreaseRef();
}

bool Classdef::hasStatic(const std::wstring& name)
{
    return instances.find(name) != instances.end();
}
    
InternalType* Classdef::getStatic(const std::wstring& name)
{
    if (hasStatic(name))
    {
        return instances[name];
    }

    return nullptr;
}

bool Classdef::setStatic(const std::wstring& name, InternalType* pIT)
{
    if (hasStatic(name))
    {
        instances[name]->DecreaseRef();
        instances[name]->killMe();

        pIT->IncreaseRef();
        instances[name] = pIT;
        return true;
    }

    return false;
}

InternalType* Classdef::instantiateProperty(const std::wstring& name, const OBJ_ATTR& attr)
{
    if (attr.arg.default_value == nullptr)
    {
        return types::Double::Empty();
    }
    else
    {
        ast::RunVisitor* exec = (ast::RunVisitor*)ConfigVariable::getDefaultVisitor();
        attr.arg.default_value->accept(*exec);
        InternalType* pIT = exec->getResult();
        if (pIT == nullptr || pIT->isAssignable() == false)
        {
            char msg[128];
            os_sprintf(msg, _("%s: Unable to evaluate default value.\n"), scilab::UTF8::toUTF8(name).data());
            throw ast::InternalError(scilab::UTF8::toWide(msg), 999, attr.arg.default_value->getLocation());
        }

        pIT->IncreaseRef();
        delete exec;
        pIT->DecreaseRef();
        return pIT;
    }
}

Callable* Classdef::instantiateMethod(const std::wstring& name, const OBJ_ATTR& attr, bool isStatic)
{
    //sciprint("instantiateMethod: %ls\n", name.data());
    Macro* m = attr.callable->getAs<Macro>();
    std::vector<symbol::Variable*>* inputs = new std::vector<symbol::Variable*>();
    for (auto&& i : *m->getInputs())
    {
        inputs->push_back(i);
    }

    std::vector<symbol::Variable*>* outputs = new std::vector<symbol::Variable*>();
    for (auto&& o : *m->getOutputs())
    {
        outputs->push_back(o);
    }

    ast::SeqExp* body = m->getBody()->clone()->getAs<ast::SeqExp>();

    if (isStatic)
    {
        return new Macro(m->getName(), this, *inputs, *outputs, *body, getName());
    }
    else
    {
        return new Macro(m->getName(), *inputs, *outputs, *body, getName());
    }
}
} // namespace types