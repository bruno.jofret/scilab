/*
 *  Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2024 - 3DS - Antoine ELIAS
 *
 * For more information, see the COPYING file which you should have received
 * along with this program.
 */

#include "arguments.hxx"
#include "overload.hxx"
#include "opexp.hxx"
#include "int.hxx"
#include "string.hxx"
#include "symbol.hxx"
#include "double.hxx"
#include "types_comparison_eq.hxx"
#include "types_comparison_lt_le_gt_ge.hxx"
#include "types_comparison_ne.hxx"
#include "tlist.hxx"
#include "colon.hxx"

extern "C"
{
#include "FileExist.h"
#include "Scierror.h"
#include "Sciwarning.h"
#include "expandPathVariable.h"
#include "graphicObjectProperties.h"
#include "hasHandleType.h"
#include "hasUIControlStyle.h"
#include "isdir.h"
}

bool mustBeEmpty(types::InternalType* x) { return x->isDouble() && x->getAs<types::Double>()->isEmpty(); }
bool mustBeDouble(types::InternalType* x) { return x->isDouble(); }
bool mustBeString(types::InternalType* x) { return x->isString(); }
bool mustBeBool(types::InternalType* x) { return x->isBool(); }
bool mustBeInt(types::InternalType* x) { return x->isInt(); }
bool mustBeInt8(types::InternalType* x) { return x->isInt8(); }
bool mustBeUInt8(types::InternalType* x) { return x->isUInt8(); }
bool mustBeInt16(types::InternalType* x) { return x->isInt16(); }
bool mustBeUInt16(types::InternalType* x) { return x->isUInt16(); }
bool mustBeInt32(types::InternalType* x) { return x->isInt32(); }
bool mustBeUInt32(types::InternalType* x) { return x->isUInt32(); }
bool mustBeInt64(types::InternalType* x) { return x->isInt64(); }
bool mustBeUInt64(types::InternalType* x) { return x->isUInt64(); }
bool mustBePoly(types::InternalType* x) { return x->isPoly(); }
bool mustBeList(types::InternalType* x) { return x->isList(); }
bool mustBeTList(types::InternalType* x) { return x->isTList(); }
bool mustBeMList(types::InternalType* x) { return x->isMList(); }
bool mustBePointer(types::InternalType* x) { return x->isPointer(); }
bool mustBeHandle(types::InternalType* x) { return x->isHandle(); }
bool mustBeStruct(types::InternalType* x) { return x->isStruct(); }
bool mustBeCell(types::InternalType* x) { return x->isCell(); }
bool mustBeLib(types::InternalType* x) { return x->isLibrary(); }
bool mustBeFunction(types::InternalType* x) { return x->isCallable(); }
bool mustBeBuiltin(types::InternalType* x) { return x->isFunction(); }
bool mustBeMacro(types::InternalType* x) { return x->isMacro() || x->isMacroFile(); }
bool mustBeSparse(types::InternalType* x) { return x->isSparse(); }
bool mustBeBoolSparse(types::InternalType* x) { return x->isSparseBool(); }
bool mustBeImplicitList(types::InternalType* x) { return x->isImplicitList(); }
template<int go>
bool mustBeHandleType(types::InternalType* x) { return x->isHandle() && hasHandleType(x->getAs<types::GraphicHandle>()->get()[0], go); }
template<int go>
bool mustBeUIControlStyle(types::InternalType* x) { return mustBeHandleType<__GO_UICONTROL__>(x) && hasUIControlStyle(x->getAs<types::GraphicHandle>()->get()[0], go); }

std::map<std::wstring, std::function<bool(types::InternalType*)>> typeValidator = {
    {L"double", mustBeDouble}, {L"constant", mustBeDouble}, {L"bool", mustBeBool}, {L"boolean", mustBeBool}, {L"string", mustBeString}, {L"int", mustBeInt}, {L"int8", mustBeInt8}, {L"uint8", mustBeUInt8}, {L"int16", mustBeInt16}, {L"uint16", mustBeUInt16}, {L"int32", mustBeInt32}, {L"uint32", mustBeUInt32}, {L"int64", mustBeInt64}, {L"uint64", mustBeUInt64}, {L"poly", mustBePoly}, {L"polynomial", mustBePoly}, {L"list", mustBeList}, {L"tlist", mustBeTList}, {L"mlist", mustBeMList}, {L"pointer", mustBePointer}, {L"handle", mustBeHandle}, {L"struct", mustBeStruct}, {L"st", mustBeStruct}, {L"cell", mustBeCell}, {L"library", mustBeLib}, {L"lib", mustBeLib}, {L"function", mustBeFunction}, {L"builtin", mustBeBuiltin}, {L"gateway", mustBeBuiltin}, {L"macro", mustBeMacro}, {L"sparse", mustBeSparse}, {L"booleansparse", mustBeBoolSparse}, {L"boolsparse", mustBeBoolSparse}, {L"implicitlist", mustBeImplicitList}, {L"range", mustBeImplicitList}, {L"axes", mustBeHandleType<__GO_AXES__>}, {L"axis", mustBeHandleType<__GO_AXIS__>}, {L"champ", mustBeHandleType<__GO_CHAMP__>}, {L"compound", mustBeHandleType<__GO_COMPOUND__>}, {L"fac3d", mustBeHandleType<__GO_FAC3D__>}, {L"fec", mustBeHandleType<__GO_FEC__>}, {L"figure", mustBeHandleType<__GO_FIGURE__>}, {L"grayplot", mustBeHandleType<__GO_GRAYPLOT__>}, {L"label", mustBeHandleType<__GO_LABEL__>}, {L"legend", mustBeHandleType<__GO_LEGEND__>}, {L"matplot", mustBeHandleType<__GO_MATPLOT__>}, {L"plot3d", mustBeHandleType<__GO_PLOT3D__>}, {L"polyline", mustBeHandleType<__GO_POLYLINE__>}, {L"rect", mustBeHandleType<__GO_RECTANGLE__>}, {L"segs", mustBeHandleType<__GO_SEGS__>}, /*{L"text", mustBeHandleType<__GO_TEXT__>},*/
    {L"uicontrol", mustBeHandleType<__GO_UICONTROL__>},
    {L"uimenu", mustBeHandleType<__GO_UIMENU__>},
    {L"checkbox", mustBeUIControlStyle<__GO_UI_CHECKBOX__>},
    {L"edit", mustBeUIControlStyle<__GO_UI_EDIT__>},
    {L"spinner", mustBeUIControlStyle<__GO_UI_SPINNER__>},
    {L"frame", mustBeUIControlStyle<__GO_UI_FRAME__>},
    {L"image", mustBeUIControlStyle<__GO_UI_IMAGE__>},
    {L"listbox", mustBeUIControlStyle<__GO_UI_LISTBOX__>},
    {L"popupmenu", mustBeUIControlStyle<__GO_UI_POPUPMENU__>},
    {L"pushbutton", mustBeUIControlStyle<__GO_UI_PUSHBUTTON__>},
    {L"radiobutton", mustBeUIControlStyle<__GO_UI_RADIOBUTTON__>},
    {L"slider", mustBeUIControlStyle<__GO_UI_SLIDER__>},
    {L"text", mustBeUIControlStyle<__GO_UI_TEXT__>},
    {L"layer", mustBeUIControlStyle<__GO_UI_LAYER__>},
    {L"tab", mustBeUIControlStyle<__GO_UI_TAB__>}};

static types::InternalType* callComparison(std::function<types::InternalType*(types::InternalType*, types::InternalType*)> cmp, ast::OpExp::Oper oper, types::InternalType* x, types::InternalType* y)
{
    types::InternalType* pIT = cmp(x, y);
    if (pIT == nullptr)
    {
        types::typed_list in = {x, y};
        types::typed_list out;
        types::Function::ReturnValue ret = Overload::generateNameAndCall(Overload::getNameFromOper(oper), in, 1, out, true);
        if (ret == types::Function::ReturnValue::OK)
        {
            return out[0];
        }
    }

    return pIT;
}

static types::InternalType* callComparison(std::function<types::InternalType*(types::InternalType*, types::InternalType*, const std::wstring&)> cmp, ast::OpExp::Oper oper, const std::wstring& operstr, types::InternalType* x, types::InternalType* y)
{
    types::InternalType* pIT = cmp(x, y, operstr);
    if (pIT == nullptr)
    {
        types::typed_list in = {x, y};
        types::typed_list out;
        types::Function::ReturnValue ret = Overload::generateNameAndCall(Overload::getNameFromOper(oper), in, 1, out, true);
        if (ret == types::Function::ReturnValue::OK)
        {
            return out[0];
        }
    }

    return pIT;
}

template<class T_OUT, class T_IN>
types::InternalType* convertNum(types::InternalType* val)
{
    T_IN* in = val->getAs<T_IN>();
    T_OUT* out = new T_OUT(in->getDims(), in->getDimsArray());
    typename T_OUT::type* pout = out->get();
    typename T_IN::type* pin = in->get();
    for (int i = 0; i < in->getSize(); ++i)
    {
        pout[i] = static_cast<typename T_OUT::type>(pin[i]);
    }

    return out;
}

template<class T_IN>
types::InternalType* convertBool(types::InternalType* val)
{
    T_IN* in = val->getAs<T_IN>();
    types::Bool* out = new types::Bool(in->getDims(), in->getDimsArray());
    int* pout = out->get();
    typename T_IN::type* pin = in->get();
    for (int i = 0; i < in->getSize(); ++i)
    {
        pout[i] = pin[i] == 0 ? 0 : 1;
    }

    return out;
}

types::InternalType* toDouble(types::InternalType* val, const std::wstring& name)
{
    switch (val->getType())
    {
        case types::InternalType::ScilabDouble:
            return val;
        case types::InternalType::ScilabBool:
            return convertNum<types::Double, types::Bool>(val);
        case types::InternalType::ScilabInt8:
            return convertNum<types::Double, types::Int8>(val);
        case types::InternalType::ScilabUInt8:
            return convertNum<types::Double, types::UInt8>(val);
        case types::InternalType::ScilabInt16:
            return convertNum<types::Double, types::Int16>(val);
        case types::InternalType::ScilabUInt16:
            return convertNum<types::Double, types::UInt16>(val);
        case types::InternalType::ScilabInt32:
            return convertNum<types::Double, types::Int32>(val);
        case types::InternalType::ScilabUInt32:
            return convertNum<types::Double, types::UInt32>(val);
        case types::InternalType::ScilabInt64:
            return convertNum<types::Double, types::Int64>(val);
        case types::InternalType::ScilabUInt64:
            return convertNum<types::Double, types::UInt64>(val);
        case types::InternalType::ScilabString:
        {
            types::String* in = val->getAs<types::String>();
            types::Double* out = new types::Double(in->getDims(), in->getDimsArray());
            double* pout = out->get();
            for (int i = 0; i < in->getSize(); ++i)
            {
                pout[i] = wcstod(in->get()[i], NULL);
            }

            return out;
        }
        default:
        {
            char msg[128];
            os_sprintf(msg, _("%ls: Unable to convert '%ls' to double.\n"), name.data(), val->getTypeStr().data());
            throw ast::InternalError(scilab::UTF8::toWide(msg));
        }
    }
}

template<class T>
types::InternalType* toInt(types::InternalType* val, const std::wstring& name)
{
    switch (val->getType())
    {
        case types::InternalType::ScilabDouble:
            return convertNum<T, types::Double>(val);
        case types::InternalType::ScilabBool:
            return convertNum<T, types::Bool>(val);
        case types::InternalType::ScilabInt8:
            return convertNum<T, types::Int8>(val);
        case types::InternalType::ScilabUInt8:
            return convertNum<T, types::UInt8>(val);
        case types::InternalType::ScilabInt16:
            return convertNum<T, types::Int16>(val);
        case types::InternalType::ScilabUInt16:
            return convertNum<T, types::UInt16>(val);
        case types::InternalType::ScilabInt32:
            return convertNum<T, types::Int32>(val);
        case types::InternalType::ScilabUInt32:
            return convertNum<T, types::UInt32>(val);
        case types::InternalType::ScilabInt64:
            return convertNum<T, types::Int64>(val);
        case types::InternalType::ScilabUInt64:
            return convertNum<T, types::UInt64>(val);
        case types::InternalType::ScilabString:
        {
            types::String* in = val->getAs<types::String>();
            T* out = new T(in->getDims(), in->getDimsArray());
            typename T::type* pout = out->get();
            for (int i = 0; i < in->getSize(); ++i)
            {
                pout[i] = static_cast<typename T::type>(std::wcstoull(in->get()[i], NULL, 10));
            }

            return out;
        }
        default:
        {
            char msg[128];
            os_sprintf(msg, _("%ls: Unable to convert '%ls' to int.\n"), name.data(), val->getTypeStr().data());
            throw ast::InternalError(scilab::UTF8::toWide(msg));
        }
    }
}

types::InternalType* toBool(types::InternalType* val, const std::wstring& name)
{
    switch (val->getType())
    {
        case types::InternalType::ScilabBool:
            return val;
        case types::InternalType::ScilabDouble:
        case types::InternalType::ScilabInt8:
        case types::InternalType::ScilabUInt8:
        case types::InternalType::ScilabInt16:
        case types::InternalType::ScilabUInt16:
        case types::InternalType::ScilabInt32:
        case types::InternalType::ScilabUInt32:
        case types::InternalType::ScilabInt64:
        case types::InternalType::ScilabUInt64:
            return toInt<types::Bool>(val, name);
        case types::InternalType::ScilabString:
        {
            types::String* in = val->getAs<types::String>();
            types::Bool* out = new types::Bool(in->getDims(), in->getDimsArray());
            int* pout = out->get();
            for (int i = 0; i < in->getSize(); ++i)
            {
                pout[i] = wcscmp(in->get()[i], L"T") == 0 ? 1 : 0;
            }

            return out;
        }
        default:
        {
            char msg[128];
            os_sprintf(msg, _("%ls: Unable to convert '%ls' to boolean.\n"), name.data(), val->getTypeStr().data());
            throw ast::InternalError(scilab::UTF8::toWide(msg));
        }
    }
}

template<class T>
types::String* toStringNum(T* in)
{
    types::String* s = new types::String(in->getDims(), in->getDimsArray());
    typename T::type* pin = in->get();

    for (int i = 0; i < in->getSize(); ++i)
    {
        s->set(i, std::to_wstring(pin[i]).c_str());
    }

    return s;
}

types::InternalType* toStringNum(types::Double* d)
{
    if (d->isEmpty())
    {
        return d;
    }

    types::String* s = new types::String(d->getDims(), d->getDimsArray());
    if (d->isComplex())
    {
        std::wostringstream ostr;
        double* pR = d->get();
        double* pI = d->getImg();
        for (int i = 0; i < d->getSize(); ++i)
        {
            DoubleComplexMatrix2String(&ostr, pR[i], pI[i]);
            s->set(i, ostr.str().c_str());
            ostr.str(L"");
        }
    }
    else
    {
        std::wostringstream ostr;
        double* pR = d->get();
        for (int i = 0; i < d->getSize(); ++i)
        {
            DoubleComplexMatrix2String(&ostr, pR[i], 0);
            s->set(i, ostr.str().c_str());
            ostr.str(L"");
        }
    }

    return s;
}

types::InternalType* toStringBool(types::Bool* b)
{
    types::String* s = new types::String(b->getDims(), b->getDimsArray());
    int* pin = b->get();
    const wchar_t* True = L"T";
    const wchar_t* False = L"F";
    for (int i = 0; i < b->getSize(); ++i)
    {
        s->set(i, pin[i] == 0 ? False : True);
    }

    return s;
}

types::InternalType* toString(types::InternalType* val, const std::wstring& name)
{
    switch (val->getType())
    {
        case types::InternalType::ScilabDouble:
            return toStringNum(val->getAs<types::Double>());
        case types::InternalType::ScilabInt8:
            return toStringNum(val->getAs<types::Int8>());
        case types::InternalType::ScilabUInt8:
            return toStringNum(val->getAs<types::UInt8>());
        case types::InternalType::ScilabInt16:
            return toStringNum(val->getAs<types::Int16>());
        case types::InternalType::ScilabUInt16:
            return toStringNum(val->getAs<types::UInt16>());
        case types::InternalType::ScilabInt32:
            return toStringNum(val->getAs<types::Int32>());
        case types::InternalType::ScilabUInt32:
            return toStringNum(val->getAs<types::UInt32>());
        case types::InternalType::ScilabInt64:
            return toStringNum(val->getAs<types::Int64>());
        case types::InternalType::ScilabUInt64:
            return toStringNum(val->getAs<types::UInt64>());
        case types::InternalType::ScilabBool:
            return toStringBool(val->getAs<types::Bool>());
        case types::InternalType::ScilabString:
            return val;
        default:
        {
            char msg[128];
            os_sprintf(msg, _("%ls: Unable to convert '%ls' to string.\n"), name.data(), val->getTypeStr().data());
            throw ast::InternalError(scilab::UTF8::toWide(msg));
        }
    }
}

std::map<std::wstring, std::function<types::InternalType*(types::InternalType*, const std::wstring& name)>> typeConvertors = {
    {L"double", toDouble},
    {L"constant", toDouble},
    {L"int", toInt<types::Int32>},
    {L"uint", toInt<types::UInt32>},
    {L"int8", toInt<types::Int8>},
    {L"uint8", toInt<types::UInt8>},
    {L"int16", toInt<types::Int16>},
    {L"uint6", toInt<types::UInt16>},
    {L"int32", toInt<types::Int32>},
    {L"uint32", toInt<types::UInt32>},
    {L"int64", toInt<types::Int64>},
    {L"uint64", toInt<types::UInt64>},
    {L"bool", toBool},
    {L"boolean", toBool},
    {L"string", toString}};

std::function<types::InternalType*(types::InternalType*, const std::wstring& name)> getTypeConvertor(const std::wstring& name)
{
    if (typeConvertors.find(name) == typeConvertors.end())
    {
        return nullptr;
    }

    return typeConvertors[name];
}

bool andBool(types::InternalType* ret)
    {
    if (ret && ret->isBool())
    {
        types::Bool* b = ret->getAs<types::Bool>();
        for (int i = 0; i < b->getSize(); ++i)
        {
            if (b->get()[i] == 0)
            {
                return false;
            }
        }
    }

    return true;
}

bool orBool(types::InternalType* ret)
{
    if (ret && ret->isBool())
    {
        types::Bool* b = ret->getAs<types::Bool>();
        for (int i = 0; i < b->getSize(); ++i)
        {
            if (b->get()[i] == 1)
            {
                return true;
            }
        }
    }

    return false;
}

int mustBePositive(types::typed_list& x)
{
    types::InternalType* tmp2 = callComparison(GenericGreater, ast::OpExp::Oper::gt, x[0], new types::Double(0));
    if (tmp2)
    {
        bool res = andBool(tmp2);
        tmp2->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeNonpositive(types::typed_list& x)
{
    types::InternalType* tmp2 = callComparison(GenericLessEqual, ast::OpExp::Oper::le, L"<=", x[0], new types::Double(0));
    if (tmp2)
    {
        bool res = andBool(tmp2);
        tmp2->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeNonnegative(types::typed_list& x)
{
    types::InternalType* tmp2 = callComparison(GenericGreaterEqual, ast::OpExp::Oper::ge, x[0], new types::Double(0));
    if (tmp2)
    {
        bool res = andBool(tmp2);
        tmp2->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeNegative(types::typed_list& x)
{
    types::InternalType* tmp2 = callComparison(GenericLess, ast::OpExp::Oper::le, L"<", x[0], new types::Double(0));
    if (tmp2)
    {
        bool res = andBool(tmp2);
        tmp2->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeNumeric(types::typed_list& x)
{
    return x[0]->isDouble() || x[0]->isInt() ? 0 : 1;
}

int mustBeFinite(types::typed_list& x)
{
    if (mustBeNumeric(x) != 0)
    {
        return 1;
    }

    if (x[0]->isDouble())
    {
        double* p = x[0]->getAs<types::Double>()->get();
        for (int i = 0; i < x[0]->getAs<types::Double>()->getSize(); ++i)
        {
            if (std::isfinite(p[i]) == false)
            {
                return 1;
            }
        }
    }
    return 0;
}

int mustBeNonNan(types::typed_list& x)
{
    if (mustBeNumeric(x) != 0)
    {
        return 1;
    }

    if (x[0]->isDouble())
    {
        double* p = x[0]->getAs<types::Double>()->get();
        for (int i = 0; i < x[0]->getAs<types::Double>()->getSize(); ++i)
        {
            if (std::isnan(p[i]))
            {
                return 1;
            }
        }
    }
    return 0;
}

int mustBeNonzero(types::typed_list& x)
{
    types::Double* tmp = new types::Double(0);
    types::InternalType* tmp2 = GenericComparisonNonEqual(x[0], tmp);
    tmp->killMe();
    if (tmp2)
    {
        bool res = andBool(tmp2);
        tmp2->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeNonsparse(types::typed_list& x)
{
    return x[0]->isSparse() ? 1 : 0;
}

static int isComplex(const double* val, size_t size, double eps)
{
    for (size_t i = 0; i < size; ++i)
    {
        if (abs(val[i]) > eps)
        {
            return 1;
        }
    }

    return 0;
}

int mustBeReal(types::typed_list& x)
{
    if (x[0]->isDouble() || x[0]->isPoly() || x[0]->isSparse())
    {
        if (x[0]->isDouble() && x[0]->getAs<types::Double>()->isComplex())
        {
            types::Double* d = x[0]->getAs<types::Double>();
            return isComplex(d->getImg(), d->getSize(), x.size() > 1 ? x[1]->getAs<types::Double>()->get()[0] : 0);
        }

        if (x[0]->isPoly() && x[0]->getAs<types::Polynom>()->isComplex())
        {
            types::Polynom* p = x[0]->getAs<types::Polynom>();
            types::Double* d = p->getCoef();
            int ret = isComplex(d->getImg(), d->getSize(), x.size() > 1 ? x[1]->getAs<types::Double>()->get()[0] : 0);
            d->killMe();
            return ret;
        }

        if (x[0]->isSparse() && x[0]->getAs<types::Sparse>()->isComplex())
        {
            types::Sparse* sp = x[0]->getAs<types::Sparse>();
            size_t nonZeros = sp->nonZeros();
            std::vector<double> NonZeroR(nonZeros);
            std::vector<double> NonZeroI(nonZeros);
            sp->outputValues(NonZeroR.data(), NonZeroI.data());

            return isComplex(NonZeroI.data(), nonZeros, x.size() > 1 ? x[1]->getAs<types::Double>()->get()[0] : 0);
        }
    }

    return 0;
}

int mustBeInteger(types::typed_list& x)
{
    if (mustBeNumeric(x) != 0)
    {
        return 1;
    }

    if (x[0]->isDouble())
    {
        double* p = x[0]->getAs<types::Double>()->get();
        for (int i = 0; i < x[0]->getAs<types::Double>()->getSize(); ++i)
        {
            if (floor(p[i]) != p[i])
            {
                return 1;
            }
        }
    }

    return 0;
}

int mustBeMember(types::typed_list& x)
{
    types::InternalType* tmp = nullptr;
    if (x[1]->isCell())
    {
        types::Cell* ce = x[1]->getAs<types::Cell>();
        types::Bool* tmp2 = new types::Bool(1, ce->getSize());
        for (int i = 0; i < ce->getSize(); ++i)
        {
            types::InternalType* tmp3 = GenericComparisonEqual(x[0], ce->get(i));
            bool res = andBool(tmp3);
            tmp2->set(i, res);
            tmp3->killMe();
        }

        tmp = tmp2;
    }
    else
    {
        tmp = GenericComparisonEqual(x[0], x[1]);
    }

    if (tmp)
    {
        bool res = orBool(tmp);
        tmp->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeGreaterThan(types::typed_list& x)
{
    types::InternalType* tmp = callComparison(GenericGreater, ast::OpExp::Oper::gt, x[0], x[1]);
    if (tmp)
    {
        bool res = andBool(tmp);
        tmp->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeGreaterThanOrEqual(types::typed_list& x)
{
    types::InternalType* tmp = callComparison(GenericGreaterEqual, ast::OpExp::Oper::ge, x[0], x[1]);
    if (tmp)
    {
        bool res = andBool(tmp);
        tmp->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeLessThan(types::typed_list& x)
{
    types::InternalType* tmp = callComparison(GenericLess, ast::OpExp::Oper::lt, L"<", x[0], x[1]);
    if (tmp)
    {
        bool res = andBool(tmp);
        tmp->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeLessThanOrEqual(types::typed_list& x)
{
    types::InternalType* tmp = callComparison(GenericLessEqual, ast::OpExp::Oper::le, L"<=", x[0], x[1]);
    if (tmp)
    {
        bool res = andBool(tmp);
        tmp->killMe();
        return res ? 0 : 1;
    }

    return 1;
}

int mustBeA(types::typed_list& x)
{
    types::String* types = x[1]->getAs<types::String>();

    for (int i = 0; i < types->getSize(); ++i)
    {
        if (typeValidator.find(types->get()[i]) != typeValidator.end())
        {
            if (typeValidator[types->get()[i]](x[0]))
            {
                return 0;
            }
        }
        else
        {
            std::wstring type;
            if (x[0]->isUserType())
            {
                type = x[0]->getAs<types::UserType>()->getTypeStr();
            }

            if (x[0]->isTList() || x[0]->isMList())
            {
                type = x[0]->getAs<types::TList>()->getTypeStr();
            }

            if (type == types->get()[i])
            {
                return 0;
            }
        }
    }

    return 1;
}

int mustBeNumericOrLogical(types::typed_list& x)
{
    return (mustBeNumeric(x) == 0 || x[0]->isBool()) ? 0 : 1;
}

int mustBeNonempty(types::typed_list& x)
{
    return (x[0]->isDouble() && x[0]->getAs<types::Double>()->isEmpty()) ? 1 : 0;
}

int mustBeScalarOrEmpty(types::typed_list& x)
{
    return (x[0]->isGenericType() && (x[0]->getAs<types::GenericType>()->getSize() == 0 || x[0]->getAs<types::GenericType>()->isScalar())) ? 0 : 1;
}

int mustBeVector(types::typed_list& x)
{
    return (x[0]->isGenericType() && x[0]->getAs<types::GenericType>()->isVector()) ? 0 : 1;
}

int mustBeSquare(types::typed_list& x)
{
    if (x[0]->isGenericType() == false)
    {
        return 1;
    }

    types::GenericType* gt = x[0]->getAs<types::GenericType>();

    if (gt->isDouble() && gt->getAs<types::Double>()->isEmpty())
    {
        return 1;
    }

    if (gt->getDims() != 2)
    {
        return 1;
    }

    int* dims = gt->getDimsArray();
    int ref = dims[0];
    for (int i = 1; i < gt->getDims(); ++i)
    {
        if (dims[i] < 1 || dims[i] != ref) //-1 0
        {
            return 1;
        }
    }

    return 0;
}

int mustBeInRange(types::typed_list& x)
{
#define checkFunc(name) [](types::InternalType* x1, types::InternalType* x2) { return name(x1, x2); }
    typedef std::function<types::InternalType*(types::InternalType*, types::InternalType*)> checker;
    checker checkLeft = checkFunc(GenericGreaterEqual);
    checker checkRight = checkFunc(GenericLessEqual);

    if (x.size() == 4)
    {
        std::wstring bounds = x[3]->getAs<types::String>()->get()[0];
        if (bounds == L"exclusive")
        {
            checkLeft = checkFunc(GenericGreater);
            checkRight = checkFunc(GenericLess);
        }
        else if (bounds == L"exclude-lower")
        {
            checkLeft = checkFunc(GenericGreater);
        }
        else if (bounds == L"exclude-upper")
        {
            checkRight = checkFunc(GenericLess);
        }
    }

    return andBool(checkLeft(x[0], x[1])) && andBool(checkRight(x[0], x[2])) ? 0 : 1;
}

int mustBeFile(types::typed_list& x)
{
    if (x[0]->isString())
    {
        wchar_t* f = x[0]->getAs<types::String>()->get()[0];
        wchar_t* e = expandPathVariableW(f);
        if (e == nullptr)
        {
            return 1;
        }

        std::wstring exp(e);
        FREE(e);
        return (isdirW(exp.data()) == false && FileExistW(exp.data())) ? 0 : 1;
    }

    return 1;
}

int mustBeFolder(types::typed_list& x)
{
    if (x[0]->isString())
    {
        wchar_t* f = x[0]->getAs<types::String>()->get()[0];
        wchar_t* e = expandPathVariableW(f);
        if (e == nullptr)
        {
            return 1;
        }

        std::wstring exp(e);
        FREE(e);
        return isdirW(exp.data()) ? 0 : 1;
    }

    return 1;
}

int mustBeNonzeroLengthText(types::typed_list& x)
{
    if (x[0]->isString() && x[0]->getAs<types::String>()->isScalar())
    {
        return wcslen(x[0]->getAs<types::String>()->get()[0]) > 0 ? 0 : 1;
    }

    return 1;
}

int mustBeValidVariableName(types::typed_list& x)
{
    if (x[0]->isString() && x[0]->getAs<types::String>()->isScalar())
    {
        return symbol::Context::getInstance()->isValidVariableName(x[0]->getAs<types::String>()->get()[0]) ? 0 : 1;
    }

    return 1;
}

int mustBeEqualDims(types::typed_list& x)
{
    types::typed_list in1 = {x[0]};
    types::typed_list out1;
    if (Overload::call(L"size", in1, 1, out1) != types::Function::OK)
    {
        return 1;
    }

    types::typed_list in2 = {x[1]};
    types::typed_list out2;
    if (Overload::call(L"size", in2, 1, out2) != types::Function::OK)
    {
        return 1;
    }

    types::Double* p1 = out1[0]->getAs<types::Double>();
    std::vector<int> dims1(p1->get(), p1->get() + p1->getSize());
    p1->killMe();

    types::Double* p2 = out2[0]->getAs<types::Double>();
    std::vector<int> dims2(p2->get(), p2->get() + p2->getSize());
    p2->killMe();

    std::vector<int> ref = {-1};
    if (x.size() == 3)
    {
        types::Double* pref = x[2]->getAs<types::Double>();
        ref.clear();
        ref.reserve(pref->getSize());
        for (int i = 0; i < pref->getSize(); ++i)
        {
            ref.push_back(static_cast<int>(pref->get()[i]));
        }
    }

    if (ref.size() >= 1 && ref[0] != -1)
    {
        for (int i = 0; i < ref.size(); ++i)
        {
            if (dims1.size() < ref[i] || dims2.size() < ref[i])
            {
                return 1;
            }
        }
    }
    else
    {
        if (dims1.size() != dims2.size())
        {
            return 1;
        }
    }

    if (ref.size() >= 1 && ref[0] != -1)
    {
        for (int i = 0; i < ref.size(); ++i)
        {
            if (dims1[ref[i] - 1] != dims2[ref[i] - 1])
            {
                return 1;
            }
        }
    }
    else
    {
        for (int i = 0; i < dims1.size(); ++i)
        {
            if (dims1[i] != dims2[i])
            {
                return 1;
            }
        }
    }

    return 0;
}

int mustBeSameType(types::typed_list& x)
{
    if (x[0]->isInt() && x[1]->isInt())
    {
        return 0;
    }

    return (x[0]->getType() == x[1]->getType()) ? 0 : 1;
}

int mustBeEqualDimsOrScalar(types::typed_list& x)
{
    int size = static_cast<int>(x.size());

    for (int i = 0; i < size - 1; ++i)
    {
        for (int j = i + 1; j < size; ++j)
        {
            // if tested is scilar => OK
            if (x[i]->getAs<types::GenericType>()->isScalar())
            {
                continue;
            }

            // if ref is scalar => OK
            if (x[j]->getAs<types::GenericType>()->isScalar())
            {
                continue;
            }

            types::typed_list tl = {x[i], x[j]};
            if (mustBeEqualDims(tl) != 0)
            {
                return j + 1;
            }
        }
    }

    return 0;
}

std::map<std::wstring, std::tuple<std::function<int(types::typed_list&)>, std::vector<int>>> functionValidators = {
    {L"mustBePositive", {mustBePositive, {1}}},
    {L"mustBeNonpositive", {mustBeNonpositive, {1}}},
    {L"mustBeNonnegative", {mustBeNonnegative, {1}}},
    {L"mustBeNegative", {mustBeNegative, {1}}},
    {L"mustBeFinite", {mustBeFinite, {1}}},
    {L"mustBeNonNan", {mustBeNonNan, {1}}},
    {L"mustBeNonzero", {mustBeNonzero, {1}}},
    {L"mustBeNonsparse", {mustBeNonsparse, {1}}},
    {L"mustBeReal", {mustBeReal, {1, 2}}},
    {L"mustBeInteger", {mustBeInteger, {1}}},
    {L"mustBeGreaterThan", {mustBeGreaterThan, {2}}},
    {L"mustBeLessThan", {mustBeLessThan, {2}}},
    {L"mustBeGreaterThanOrEqual", {mustBeGreaterThanOrEqual, {2}}},
    {L"mustBeLessThanOrEqual", {mustBeLessThanOrEqual, {2}}},
    {L"mustBeA", {mustBeA, {2}}},
    {L"mustBeNumeric", {mustBeNumeric, {1}}},
    {L"mustBeNumericOrLogical", {mustBeNumericOrLogical, {1}}},
    {L"mustBeNumericOrBoolean", {mustBeNumericOrLogical, {1}}},
    {L"mustBeNonempty", {mustBeNonempty, {1}}},
    {L"mustBeScalarOrEmpty", {mustBeScalarOrEmpty, {1}}},
    {L"mustBeVector", {mustBeVector, {1}}},
    {L"mustBeSquare", {mustBeSquare, {1}}},
    {L"mustBeMember", {mustBeMember, {2}}},
    {L"mustBeInRange", {mustBeInRange, {3, 4}}},
    {L"mustBeFile", {mustBeFile, {1}}},
    {L"mustBeFolder", {mustBeFolder, {1}}},
    {L"mustBeNonzeroLengthText", {mustBeNonzeroLengthText, {1}}},
    {L"mustBeValidVariableName", {mustBeValidVariableName, {1}}},
    {L"mustBeEqualDims", {mustBeEqualDims, {2, 3}}},
    {L"mustBeSameType", {mustBeSameType, {2}}},
    {L"mustBeEqualDimsOrScalar", {mustBeEqualDimsOrScalar, {-1}}}};

std::tuple<std::function<int(types::typed_list&)>, std::vector<int>> getFunctionValidator(const std::wstring& name)
{
    if (functionValidators.find(name) == functionValidators.end())
    {
        return {nullptr, {0}};
    }

    return functionValidators[name];
}

std::map<std::wstring, std::tuple<std::string, int>> errorValidators = {
    {L"mustBePositive", {"%s: Wrong value for input argument #%d: Positive numbers expected.\n", 2}},
    {L"mustBeNonpositive", {"%s: Wrong value for input argument #%d: Non positive numbers expected.\n", 2}},
    {L"mustBeNonnegative", {"%s: Wrong value for input argument #%d: Non negative numbers expected.\n", 2}},
    {L"mustBeNegative", {"%s: Wrong value for input argument #%d: Negative numbers expected.\n", 2}},
    {L"mustBeFinite", {"%s: Wrong value for input argument #%d: Finite numbers expected.\n", 2}},
    {L"mustBeNonNan", {"%s: Wrong value for input argument #%d: Nan are not allowed.\n", 2}},
    {L"mustBeNonzero", {"%s: Wrong value for input argument #%d: Zero are not allowed.\n", 2}},
    {L"mustBeNonsparse", {"%s: Wrong value for input argument #%d: Sparse are not allowed.\n", 2}},
    {L"mustBeReal", {"%s: Wrong value for input argument #%d: Real numbers expected.\n", 2}},
    {L"mustBeInteger", {"%s: Wrong value for input argument #%d: Integer numbers expected.\n", 2}},
    {L"mustBeMember", {"%s: Wrong value for input argument #%d: Must be in %s.\n", 3}},
    {L"mustBeGreaterThan", {"%s: Wrong value for input argument #%d: Must be > %s.\n", 3}},
    {L"mustBeGreaterThanOrEqual", {"%s: Wrong value for input argument #%d: Must be >= %s.\n", 3}},
    {L"mustBeLessThan", {"%s: Wrong value for input argument #%d: Must be < %s.\n", 3}},
    {L"mustBeLessThanOrEqual", {"%s: Wrong value for input argument #%d: Must be <= %s.\n", 3}},
    {L"mustBeA", {"%s: Wrong type for input argument #%d: Must be in %s.\n", 3}},
    {L"mustBeNumeric", {"%s: Wrong type for input argument #%d: Must be numeric values.\n", 2}},
    {L"mustBeNumericOrLogical", {"%s: Wrong type for input argument #%d: Must be numeric values or boolean.\n", 2}},
    {L"mustBeNumericOrBoolean", {"%s: Wrong type for input argument #%d: Must be numeric values or boolean.\n", 2}},
    {L"mustBeNonempty", {"%s: Wrong type for input argument #%d: Must not be empty.\n", 2}},
    {L"mustBeScalarOrEmpty", {"%s: Wrong type for input argument #%d: Must be a scalar or empty.\n", 2}},
    {L"mustBeVector", {"%s: Wrong type for input argument #%d: Must be a vector.\n", 2}},
    {L"mustBeSquare", {"%s: Wrong type for input argument #%d: Must be a square matrix.\n", 2}},
    {L"mustBeMember", {"%s: Wrong type for input argument #%d: Must be member of %s.\n", 3}},
    {L"mustBeInRange", {"%s: Wrong value for input argument #%d: Must be in range [%s, %s].\n", 4}},
    {L"mustBeFile", {"%s: Wrong type for input argument #%d: Must be a file.\n", 2}},
    {L"mustBeFolder", {"%s: Wrong type for input argument #%d: Must be a folder.\n", 2}},
    {L"mustBeNonzeroLengthText", {"%s: Wrong type for input argument #%d: Must not be an empty string.\n", 2}},
    {L"mustBeValidVariableName", {"%s: Wrong type for input argument #%d: Must be a valid variable name.\n", 2}},
    {L"mustBeEqualDims", {"%s: Wrong size for input argument #%d: Must be of the same dimensions of #%s.\n", 3}},
    {L"mustBeSameType", {"%s: Wrong type for input argument #%d: Must be same type of #%s.\n", 3}},
    {L"mustBeEqualDimsOrScalar", {"%s: Wrong size for input argument #%d: Must be of the same dimensions of #%s or scalar.\n", -3}},
};

std::tuple<std::string, int> getErrorValidator(const std::wstring& name)
{
    return errorValidators[name];
}

std::map<std::wstring, std::vector<std::tuple<int, std::string>>> errorArgs = {
    {L"mustBeMember", {{1, ""}}},
    {L"mustBeGreaterThan", {{1, ""}}},
    {L"mustBeGreaterThanOrEqual", {{1, ""}}},
    {L"mustBeLessThan", {{1, ""}}},
    {L"mustBeLessThanOrEqual", {{1, ""}}},
    {L"mustBeA", {{1, ""}}},
    {L"mustBeInRange", {{1, ""}, {2, ""}}},
    {L"mustBeEqualDims", {{1, ""}}},
    {L"mustBeSameType", {{1, ""}}},
    {L"mustBeEqualDimsOrScalar", {{1, ""}}},
};

std::vector<std::tuple<int, std::string>> getErrorArgs(const std::wstring& name)
{
    return errorArgs[name];
}

types::InternalType* transposevar(types::InternalType* x, const std::vector<std::tuple<std::vector<int>, symbol::Variable*>>& dims)
{
    if (x->isArrayOf())
    {
        types::GenericType* gt = x->getAs<types::GenericType>();
        // if dims == (1, x) ou (x, 1) & input is a vector, transpose it to match good shape
        if (dims.size() == 2)
        {
            std::vector<int> transposeDims;

            for (int i = 0; i < 2; ++i)
            {
                std::vector<int> dim;
                symbol::Variable* v;
                std::tie(dim, v) = dims[i];

                if (v != nullptr)
                {
                    types::InternalType* pIT = v->get();
                    if (pIT && pIT->isDouble())
                    {
                        types::Double* d = pIT->getAs<types::Double>();
                        if (d->isScalar())
                        {
                            transposeDims.push_back(static_cast<int>(d->get()[0]));
                        }
                    }
                }
                else
                {
                    if (dim.size() == 1)
                    {
                        transposeDims.push_back(dim[0]);
                    }
                }
            }

            if (transposeDims.size() == 2)
            {
                if (transposeDims[0] == 1)
                {
                    if (transposeDims[1] == -1 || transposeDims[1] == gt->getRows())
                    {
                        types::InternalType* transposed;
                        gt->transpose(transposed);
                        return transposed;
                    }
                }
                else if (transposeDims[1] == 1)
                {
                    if (transposeDims[0] == -1 || transposeDims[0] == gt->getCols())
                    {
                        types::InternalType* transposed;
                        gt->transpose(transposed);
                        return transposed;
                    }
                }
            }
        }
    }

    return nullptr;
}

types::InternalType* expandvar(types::InternalType* x, const std::vector<std::tuple<std::vector<int>, symbol::Variable*>>& dims, bool isStatic)
{
    if (x->isArrayOf())
    {
        std::vector<int> convertDims;
        types::GenericType* gt = x->getAs<types::GenericType>();
        if (gt->isScalar())
        {
            if (isStatic)
            {
                for (auto&& d : dims)
                {
                    convertDims.push_back(std::get<0>(d)[0]);
                }
            }
            else
            {
                for (auto&& d : dims)
                {
                    std::vector<int> dim;
                    symbol::Variable* v;
                    std::tie(dim, v) = d;
                    if (v == nullptr)
                    {
                        if (dim.size() == 1 && dim[0] != -1)
                        {
                            convertDims.push_back(dim[0]);
                        }
                        else
                        {
                            convertDims.clear();
                        }
                    }
                    else
                    {
                        types::InternalType* pIT = v->get();
                        if (pIT && pIT->isDouble())
                        {
                            types::Double* d = pIT->getAs<types::Double>();
                            if (d->isScalar())
                            {
                                convertDims.push_back(static_cast<int>(d->get()[0]));
                            }
                            else
                            {
                                convertDims.clear();
                            }
                        }
                        else
                        {
                            convertDims.clear();
                        }
                    }
                }
            }
        }

        if (convertDims.size() != 0)
        {
            int size = 1;
            std::for_each(convertDims.begin(), convertDims.end(), [&size](int v)
                          { size *= v; });

            if (size != 1)
            {
                // clone to keep the same type of input argument
                types::GenericType* clone = gt->clone();
                clone->resize(convertDims.data(), static_cast<int>(convertDims.size()));

                types::typed_list in;
                types::InternalType* colon = new types::Colon();
                in.push_back(colon);

                clone = clone->insert(&in, gt);
                colon->killMe();
                return clone;
            }
        }
    }

    return x;
}

types::InternalType* checksize(types::InternalType* x, const std::vector<std::tuple<std::vector<int>, symbol::Variable*>>& dims, bool isStatic)
{
    if (x->isGenericType() == false)
    {
        return nullptr;
    }

    types::GenericType* g = x->getAs<types::GenericType>();

    types::typed_list in1 = {x};
    types::typed_list out1;
    if (Overload::call(L"size", in1, 1, out1) != types::Function::OK)
    {
        return nullptr;
    }

    types::Double* p1 = out1[0]->getAs<types::Double>();
    std::vector<int> dims1(p1->get(), p1->get() + p1->getSize());
    p1->killMe();

    if (dims1.size() > dims.size())
    {
        return nullptr;
    }

    if (dims.size() == 1 && std::get<1>(dims[0]) == nullptr)
    {
        auto&& d = std::get<0>(dims[0]);
        for (int i = 0; i < d.size(); ++i)
        {
            if (d[i] == dims1[0])
            {
                return x;
            }
        }

        return nullptr;
    }

    if (g->isScalar())
    {
        return expandvar(x, dims, isStatic);
    }

    bool status = true;
    for (int i = 0; i < dims.size(); ++i)
    {
        std::vector<int> dim;
        symbol::Variable* v;
        std::tie(dim, v) = dims[i];
        int ref = i < dims1.size() ? dims1[i] : 1;
        bool ok = false;
        if (v != nullptr)
        {
            types::InternalType* pIT = v->get();
            if (pIT && pIT->isDouble())
            {
                types::Double* d = pIT->getAs<types::Double>();

                for (int j = 0; j < d->getSize(); ++j)
                {
                    if (d->get()[j] == ref)
                    {
                        ok = true;
                        break;
                    }
                }
            }
        }
        else
        {
            for (int j = 0; j < dim.size(); ++j)
            {
                if (dim[j] == -1 || dim[j] == ref)
                {
                    ok = true;
                    break;
                }
            }
        }

        status &= ok;

        if (ok == false && dims1[i] == 1)
        {
            return transposevar(x, dims);
        }
    }

    if (status)
    {
        return x;
    }

    return nullptr;
}

std::vector<std::vector<int>> todims(const std::vector<std::tuple<std::vector<int>, symbol::Variable*>>& dims)
{
    std::vector<std::vector<int>> ret;
    for (auto&& s : dims)
    {
        std::vector<int> d;
        symbol::Variable* v;
        std::tie(d, v) = s;
        if (v == nullptr)
        {
            ret.push_back(d);
        }
        else
        {
            ret.push_back({static_cast<int>(v->top()->m_pIT->getAs<types::Double>()->get()[0])});
        }
    }

    return ret;
}

std::wstring dims2str(const std::vector<std::tuple<std::vector<int>, symbol::Variable*>>& dims)
{
    std::wstring res = L"";

    std::vector<std::vector<int>> c = todims(dims);
    for (int i = 0; i < c.size(); ++i)
    {
        auto s = c[i];
        if (res.empty() == false)
        {
            res += L" x ";
        }

        if (s.size() == 1)
        {
            res += s[0] == -1 ? std::wstring(1, L'm' + i) : std::to_wstring(s[0]);
        }
        else
        {
            std::wstring res2;
            for (auto&& d : s)
            {
                if (res2.empty() == false)
                {
                    res2 += L", ";
                }

                res2 += std::to_wstring(d);
            }

            res += L"[" + res2 + L"]";
        }
    }

    return res;
}
