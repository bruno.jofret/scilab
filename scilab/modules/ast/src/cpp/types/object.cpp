/*
 *  Scilab ( https://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2024 - 3DS - Antoine ELIAS
 *
 * For more information, see the COPYING file which you should have received
 * along with this program.
 */

#include "object.hxx"
#include "macro.hxx"

extern "C"
{
#include "sciprint.h"
}

namespace types
{

    Object::Object(Classdef* classdef) : def(classdef)
    {
        loadClassdef(classdef);
        addHelpers();
    }

    //copy cstor called by clone
    Object::Object(const Object& obj)
    {
        //sciprint("clone obj\n");
        def = obj.getClassdef();
        loadClassdef(def);
        for (auto&& prop : obj.getProperties())
        {
            InternalType* old = properties[prop.first];
            old->DecreaseRef();
            old->killMe();

            InternalType* val = prop.second->clone();
            properties[prop.first] = val;
            val->IncreaseRef();
        }

        addHelpers();
    }

    void Object::addHelper(const std::wstring& name, Function::ReturnValue(Object::* _pFunc)(typed_list&, int, typed_list&))
    {
        if (getMethod(name) == nullptr)
        {
            methods[name] = Function::createFunction(name, this, _pFunc, def->getName());
            hidden.insert(name);
        }
    }

    Object::~Object()
    {
        //sciprint("delete %ls\n", def->getName().data());
        typed_list in;
        optional_list opt;
        typed_list out;
        IncreaseRef();
        callMethod(L"delete", in, opt, 0, out);
        DecreaseRef();
        for (auto&& prop : properties)
        {
            prop.second->DecreaseRef();
            prop.second->killMe();
        }

        for (auto&& method : methods)
        {
            method.second->DecreaseRef();
            method.second->killMe();
        }

        for (auto&& i : instances)
        {
            i.second->DecreaseRef();
            i.second->killMe();
        }
    }

    void Object::loadClassdef(Classdef* def, int level)
    {
        // superclass
        for (auto&& s : def->getSuperclass())
        {
            Classdef* c = std::get<1>(s);
            Object* i = new Object(c);
            i->IncreaseRef();
            instances[c->getName()] = i;
        }

        for (auto&& prop : def->getProperties())
        {
            if (prop.second.isStatic == false)
            {
                // remove previous instance from superclass
                if (properties.find(prop.first) != properties.end())
                {
                    properties[prop.first]->DecreaseRef();
                    properties[prop.first]->killMe();
                }

                properties[prop.first] = def->instantiateProperty(prop.first, prop.second);
                properties[prop.first]->IncreaseRef();
            }
        }

        for (auto&& method : def->getMethods())
        {
            if (method.second.isStatic == false)
            {
                // remove previous instance from superclass
                if (methods.find(method.first) != methods.end())
                {
                    methods[method.first]->DecreaseRef();
                    methods[method.first]->killMe();
                }

                // hide constructor for completion (obj.[TAB])
                if (method.first == def->getName())
                {
                    hidden.insert(method.first);
                }

                Macro* macro = def->instantiateMethod(method.first, method.second)->getAs<Macro>();
                macro->setParent(this);
                macro->IncreaseRef();
                methods[method.first] = macro;
            }
        }
    }

bool Object::invoke(typed_list& in, optional_list& /*opt*/, int /*_iRetCount*/, typed_list& out, const ast::Exp& /*e*/)
{
    return true;
}

bool Object::extract(const std::wstring& name, InternalType*& out)
{
    AccessModifier access;
    if (getAccessProperty(name, access))
    {
        if (access == AccessModifier::PUBLIC || (symbol::Context::getInstance()->getCurrentObject() == this && access != AccessModifier::NONE))
        {
            out = getProperty(name);
            if (out == nullptr)
            {
                out = def->getStatic(name);
            }
            return true;
        }
        else
        {
            wchar_t szError[128];
            os_swprintf(szError, 128, _W("Wrong extraction: property '%ls' is not accessible.\n").c_str(), name.data());
            throw ast::InternalError(szError);
        }
    }

    if (getAccessMethod(name, access))
    {
        if (access == AccessModifier::PUBLIC || (symbol::Context::getInstance()->getCurrentObject() == this && access != AccessModifier::NONE))
        {
            out = getMethod(name);
            return true;
        }
        else
        {
            wchar_t szError[128];
            os_swprintf(szError, 128, _W("Wrong extraction: method '%ls' is not accessible.\n").c_str(), name.data());
            throw ast::InternalError(szError);
        }
    }
    
    return false;
}

Object* Object::insert(typed_list* _pArgs, InternalType* _pSource)
{
    // update of property
    if (_pArgs->size() == 1 && (*_pArgs)[0]->isString())
    {
        std::wstring field((*_pArgs)[0]->getAs<types::String>()->get(0));
        AccessModifier access;
        if (getAccessProperty(field, access))
        {

            if (access == AccessModifier::PUBLIC || (symbol::Context::getInstance()->getCurrentObject() == this && access != AccessModifier::NONE))
            {
                if (properties.find(field) != properties.end())
                {
                    InternalType* old = properties[field];
                    old->DecreaseRef();
                    old->killMe();

                    properties[field] = _pSource;
                    _pSource->IncreaseRef();
                    return this;
                }
                else
                {
                    if (def->setStatic(field, _pSource))
                    {
                        return this;
                    }
                }

                for (auto&& i : instances)
                {
                    try
                    {
                        return i.second->insert(_pArgs, _pSource);
                    }
                    catch (ast::InternalError& e)
                    {
                        continue;
                    }
                }

                wchar_t szError[128];
                os_swprintf(szError, 128, _W("Wrong insertion: property '%ls' does not exist.\n").c_str(), field.data());
                throw ast::InternalError(szError);
            }
            else
            {
                wchar_t szError[128];
                os_swprintf(szError, 128, _W("Wrong insertion: property '%ls' is not accessible.\n").c_str(), field.data());
                throw ast::InternalError(szError);
            }
        }
        else
        {
            wchar_t szError[128];
            os_swprintf(szError, 128, _W("Wrong insertion: property '%ls' does not exist.\n").c_str(), field.data());
            throw ast::InternalError(szError);
        }
    }

    //insertion
    //obj(1,2) = 42
    if (hasMethod(L"insert"))
    {
        typed_list in;
        optional_list opt;
        typed_list out;

        for (auto&& p : *_pArgs)
        {
            in.push_back(p);
            p->IncreaseRef();
        }

        in.push_back(_pSource);
        _pSource->IncreaseRef();

        Function::ReturnValue ret = callMethod(L"insert", in, opt, 0, out);
        for (auto&& p : *_pArgs)
        {
            p->DecreaseRef();
        }

        _pSource->DecreaseRef();

        return this;
    }

    wchar_t szError[bsiz];
    os_swprintf(szError, bsiz, _W("Wrong insertion: method '%ls' does not exist.\n").c_str(), L"insert");
    throw ast::InternalError(szError);
}

Function::ReturnValue Object::callConstructor(typed_list& in, optional_list& opt, int _iRetCount, typed_list& out)
{
    for (auto&& s : def->getSuperclass())
    {
        if (callSuperclassContructor(std::get<1>(s), in, opt, _iRetCount, out) == Function::Error)
        {
            return Function::Error;
        }
    }

    Function::ReturnValue ret = callMethod(def->getName(), in, opt, _iRetCount, out);
    if (ret == Function::Error)
    {
        return Function::Error;
    }

    return Function::OK;
}

Function::ReturnValue Object::callSuperclassContructor(Classdef* super, typed_list & in, optional_list& opt, int _iRetCount, typed_list& out)
{
    for (auto&& s : super->getSuperclass())
    {
        callSuperclassContructor(std::get<1>(s), in, opt, _iRetCount, out);
    }

    Function::ReturnValue ret = callMethod(super->getName(), in, opt, _iRetCount, out);
    if (ret == Function::Error)
    {
        return Function::Error;
    }

    return Function::OK;
}

Function::ReturnValue Object::callMethod(const std::wstring& method, typed_list& in, optional_list& opt, int _iRetCount, typed_list& out)
{
    Callable* call = getMethod(method);
    if (call == nullptr)
    {
        // function not found
        return Function::OK_NoResult;
    }

    scope.push(def->getMethodClassdef(method));
    call->getAs<Macro>()->setParent(this);

    Function::ReturnValue res;
    try
    {
        res = call->call(in, opt, _iRetCount, out);
    }
    catch (ast::InternalError& e)
    {
        scope.pop();
        throw e;
    }

    scope.pop();
    return res;
}

Object* Object::getInstance(const std::wstring& name)
{
    auto it = instances.find(name);
    if (it != instances.end())
    {
        return it->second;
    }

    for (auto&& i : instances)
    {
        Object* o = i.second->getInstance(name);
        if (o)
        {
            return o;
        }
    }

    return nullptr;
}

bool Object::hasMethod(const std::wstring& method)
{
    return getMethod(method) != nullptr;
}

Callable* Object::getMethod(const std::wstring& name)
{
    std::map<std::wstring, Object*>::iterator it = instances.begin();
    Object* o = this;
    if (scope.size() == 0 || scope.top() == L"" || scope.top() == def->getName())
    {
        if (methods.find(name) != methods.end())
        {
            return methods[name];
        }

        for (auto&& i : instances)
        {
            Callable* call = i.second->getMethod(name);
            if (call)
            {
                return call;
            }
        }
    }
    else
    {
        o = getInstance(scope.top());
        if (o)
        {
            return o->getMethod(name);
        }
    }

    return nullptr;
}

bool types::Object::getAccessMethod(const std::wstring& name, AccessModifier& access)
{
    if (scope.size() == 0 || scope.top() == L"" || scope.top() == def->getName())
    {
        auto props = def->getMethods();
        auto p = props.find(name);
        if (p != props.end())
        {
            access = p->second.access;
            return true;
        }

        for (auto&& i : instances)
        {
            if (i.second->getAccessMethod(name, access))
            {
                switch (access)
                {
                    case types::PRIVATE:
                        access = AccessModifier::NONE;
                        break;
                    case types::PROTECTED:
                        access = AccessModifier::PRIVATE;
                        break;
                }

                return true;
            }
        }
    }
    else
    {
        Object* o = getInstance(scope.top());
        if (o)
        {
            return o->getAccessMethod(name, access);
        }
    }

    return false;
}

bool Object::hasProperty(const std::wstring& property)
{
    return getProperty(property) != nullptr;
}

InternalType* Object::getProperty(const std::wstring& name)
{
    std::map<std::wstring, Object*>::iterator it = instances.begin();
    Object* o = this;
    if (scope.size() == 0 || scope.top() == L"" || scope.top() == def->getName())
    {
        if (properties.find(name) != properties.end())
        {
            return properties[name];
        }

        for (auto&& i : instances)
        {
            InternalType* pIT = i.second->getProperty(name);
            if (pIT)
            {
                return pIT;
            }
        }
    }
    else
    {
        o = getInstance(scope.top());
        if (o)
        {
            return o->getProperty(name);
        }
    }

    return nullptr;
}

bool types::Object::getAccessProperty(const std::wstring& name, AccessModifier& access)
{
    if (scope.size() == 0 || scope.top() == L"" || scope.top() == def->getName())
    {
        auto props = def->getProperties();
        auto p = props.find(name);
        if (p != props.end())
        {
            access = p->second.access;
            return true;
        }

        for (auto&& i : instances)
        {
            if (i.second->getAccessProperty(name, access))
            {
                switch (access)
                {
                    case types::PRIVATE:
                        access = AccessModifier::NONE;
                        break;
                    case types::PROTECTED:
                        access = AccessModifier::PRIVATE;
                        break;
                }

                return true;
            }
        }
    }
    else
    {
        Object* o = getInstance(scope.top());
        if (o)
        {
            return o->getAccessProperty(name, access);
        }
    }

    return false;
}

types::String* Object::getFields()
{
    std::vector<std::wstring> fields;

    for (auto&& p : properties)
    {
        if (hidden.find(p.first) == hidden.end())
        {
            fields.push_back(p.first);
        }
    }

    for (auto&& m : methods)
    {
        if (hidden.find(m.first) == hidden.end())
        {
            fields.push_back(m.first);
        }
    }

    types::String* pFields = new types::String(fields.size(), 1);
    for (int i = 0; i < fields.size(); ++i)
    {
        pFields->set(i, fields[i].data());
    }

    return pFields;
}

void Object::addHelpers()
{
    // add undefined useful overload
    addHelper(L"disp", &Object::object_disp);
    addHelper(L"outline", &Object::object_outline);
    addHelper(L"eq", &Object::object_eq);
    addHelper(L"ne", &Object::object_ne);
    addHelper(L"clone", &Object::object_clone);
    addHelper(L"delete", &Object::object_delete);
}

/* gateways for default overload*/
types::Function::ReturnValue Object::object_disp(types::typed_list& in, int _iRetCount, types::typed_list& out)
{
    return types::Function::OK;
}

types::Function::ReturnValue Object::object_outline(types::typed_list& in, int _iRetCount, types::typed_list& out)
{
    std::wostringstream ostr;
    ostr << " " << def->getName() << " object";
    out.push_back(new types::String(ostr.str().data()));
    return types::Function::OK;
}

types::Function::ReturnValue Object::object_eq(types::typed_list& in, int _iRetCount, types::typed_list& out)
{
    //sciprint("Object::object_eq\n");
    out.push_back(new types::Bool(in[0] == in[1]));
    return types::Function::OK;
}

types::Function::ReturnValue Object::object_ne(types::typed_list& in, int _iRetCount, types::typed_list& out)
{
    //sciprint("Object::object_ne\n");
    out.push_back(new types::Bool(in[0] != in[1]));
    return types::Function::OK;
}

types::Function::ReturnValue Object::object_clone(types::typed_list& in, int _iRetCount, types::typed_list& out)
{
    //sciprint("Object::object_clone\n");
    out.push_back(new types::Object(*this));
    return types::Function::OK;
}

types::Function::ReturnValue Object::object_delete(types::typed_list& in, int _iRetCount, types::typed_list& out)
{
    //sciprint("Object::object_delete\n");
    return types::Function::OK;
}
} // namespace types
